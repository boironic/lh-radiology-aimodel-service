from flask import Flask
from flask_restful import Api
from apispec import APISpec

from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec

from resources.aimodel import aimodel
from resources.cheXnet import cheXnet
from resources.yolox import YOLOX
from resources.updateCheXnet import updateCheXnet
from common.model_update.model_update_scheduler import model_update_scheduled_task
from common.config.config import Config as cfg

from flask_apscheduler import APScheduler
import atexit


def create_app():
    app = Flask(__name__)
    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()
    # app.config.from_pyfile(config_filename)

    # Scan directory for model update
    app.apscheduler.add_job(func=model_update_scheduled_task, trigger='interval', seconds=cfg.MODEL_UPDATE_SCHEDULER_INTERVAL, id='model_update_scheduler')

    api = Api(app)

    app.config.update({
        'APISPEC_SPEC': APISpec(
            title='AI Model Service',
            version='v1',
            plugins=[MarshmallowPlugin()],
            openapi_version='2.0'
        ),
        'APISPEC_SWAGGER_URL': '/swagger/',  # URI to access API Doc JSON
        'APISPEC_SWAGGER_UI_URL': '/swagger-ui/'  # URI to access UI of API Doc
    })

    docs = FlaskApiSpec(app)

    api.add_resource(aimodel, '/models')
    docs.register(aimodel)

    api.add_resource(cheXnet, '/bounding-box')
    docs.register(cheXnet)

    api.add_resource(YOLOX, '/coco-detection')
    docs.register(YOLOX)

    api.add_resource(updateCheXnet, '/model-update')
    docs.register(updateCheXnet)

    # Shut down the scheduler when exiting the app
    atexit.register(lambda: scheduler.shutdown())
    
    return app

if __name__ == '__main__':
    app = create_app()
    app.run(debug=True, host="0.0.0.0", port=5000, use_reloader=False)
