import os
import sys
import math
import copy
import random
import warnings
import pandas as pd
import numpy as np
import logging
import cv2
import csv
import time
import random
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optin
import torch.backends.cudnn as cudnn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as tfunc
import torch.multiprocessing as mp

from torch.autograd import Variable
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
from torch.utils.data.dataset import random_split
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn.parallel import DistributedDataParallel as DDP
from PIL import Image

from sklearn.metrics import roc_auc_score, confusion_matrix
from tqdm import tqdm, trange
import sklearn.metrics as metrics


from torch.utils.data import DataLoader, RandomSampler, TensorDataset
import torch.optim as optim
from sklearn.metrics import roc_auc_score

sample_train_data = pd.read_csv("../../../sample_train_data_l.csv")

nnIsTrained = False
nnClassCount = 14

trBatchSize = 64
trMaxEpoch = 3

imgtransResize = (320, 320)
imgtransCrop = 224

normalize = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
transformList = []
transformList.append(transforms.RandomResizedCrop(imgtransCrop))
transformList.append(transforms.RandomHorizontalFlip())
transformList.append(transforms.ToTensor())
transformList.append(normalize)      
transformSequence=transforms.Compose(transformList)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

if device == "cuda:0":
    device_gpu = True
else:
    device_gpu = False
    
def make_train_triplets(pathology, sample_train_data, n):
    '''
    Returns 'n' triplet images for training data for a given pathology.
    First Image - Any random image (with atleast 40 images which has the pathology)
    Second Image - An image which is positive for the pathology.
    Third Image - An image which is negative for the pathology.
    '''
    
    actual_path = pathology
    pred_path = 'pred_'+pathology
    false_negative = sample_train_data[(sample_train_data[actual_path] == 1.0) & (sample_train_data[pred_path] == 0.0)]
    false_positive = sample_train_data[(sample_train_data[actual_path] == 0.0) & (sample_train_data[pred_path] == 1.0)]
    print ('False Negatives - {} False Positives - {}'.format(len(false_negative), len(false_positive)))

    positive = sample_train_data[sample_train_data[actual_path] == 1.0]
    negative = sample_train_data[sample_train_data[actual_path] == 0.0]
    
    images = []
    checking_label = []
    
    for i in range(n):
        if (len(false_negative.index) > 0 and len(false_positive.index) > 0):
            choose = random.choice(['a','b'])    
            if (choose == 'a'):
                first_image_index = random.choice(false_negative.index)
                checking_label.append(-1)
            elif (choose == 'b'):
                first_image_index = random.choice(false_positive.index)
                checking_label.append(1)
        else:
            if (len(false_negative.index) > 0):
                first_image_index = random.choice(false_negative.index)
                checking_label.append(-1)
            elif (len(false_positive.index) > 0):
                first_image_index = random.choice(false_positive.index)
                checking_label.append(1)
            
        second_image_index = random.choice(positive.index)
        third_image_index = random.choice(negative.index)

        # Convert the indices to images
        first_image = sample_train_data['image_path'][first_image_index]
        first_image = Image.open(first_image).convert('RGB')
        first_image = transformSequence(first_image)
        
        second_image = sample_train_data['image_path'][second_image_index]
        second_image = Image.open(second_image).convert('RGB')
        second_image = transformSequence(second_image)
        
        third_image = sample_train_data['image_path'][third_image_index]
        third_image = Image.open(third_image).convert('RGB')
        third_image = transformSequence(third_image)
        
        images += [[first_image, second_image, third_image]]
        
    return (images, checking_label)

def make_test_triplets(pathology, sample_train_data):
    '''
    - Returns three images (so the name triplets)
    - First Image : An Inference failed image. (Inference failures can appear in the form of false negatives and 
    false postives)
    - Second Image: An image which is positive for the pathology.
    - Third Image: An image which is negative for the pathology.
    '''
    
    actual_path = pathology
    pred_path = 'pred_'+pathology
    false_negative = sample_train_data[(sample_train_data[actual_path] == 1.0) & (sample_train_data[pred_path] == 0.0)]
    false_positive = sample_train_data[(sample_train_data[actual_path] == 0.0) & (sample_train_data[pred_path] == 1.0)]
    print ('False Negatives - {} False Positives - {}'.format(len(false_negative), len(false_positive)))

    positive = sample_train_data[sample_train_data[actual_path] == 1.0]
    negative = sample_train_data[sample_train_data[actual_path] == 0.0]
    
    images = []
    checking_label = []
    
    false_inference_index = false_negative.index.union(false_positive.index)
    for index in false_inference_index:
        if index in false_negative.index:
            checking_label.append(-1)
        elif index in false_positive.index:
            checking_label.append(1)
            
        first_image_index  = index    
        second_image_index = random.choice(positive.index)
        third_image_index = random.choice(negative.index)

        # Convert the indices to images
        first_image = sample_train_data['image_path'][first_image_index]
        first_image = Image.open(first_image).convert('RGB')
        first_image = transformSequence(first_image)

        second_image = sample_train_data['image_path'][second_image_index]
        second_image = Image.open(second_image).convert('RGB')
        second_image = transformSequence(second_image)

        third_image = sample_train_data['image_path'][third_image_index]
        third_image = Image.open(third_image).convert('RGB')
        third_image = transformSequence(third_image)
        
        print (first_image_index, second_image_index, third_image_index)
        images += [[first_image, second_image, third_image]]
        
    return (images, checking_label, false_inference_index)


def ref_train_image_triplets(images, checking_label):
    first_images = [i[0] for i in images]
    second_images = [i[1] for i in images]
    third_images = [i[2] for i in images]

    first_images = torch.stack(first_images)
    second_images = torch.stack(second_images)
    third_images = torch.stack(third_images)
    
    target = torch.Tensor(checking_label).int()    
    return (first_images, second_images, third_images, target)


def ref_test_image_triplets(test_images, test_checking_label):
    test_first_images = [i[0] for i in test_images]
    test_second_images = [i[1] for i in test_images]
    test_third_images = [i[2] for i in test_images]
    
    test_first_images = torch.stack(test_first_images)
    test_second_images = torch.stack(test_second_images)
    test_third_images = torch.stack(test_third_images)
    
    test_target = torch.Tensor(test_checking_label).int()
    return (test_first_images, test_second_images, test_third_images, test_target)

def triplet_distance(anchor, positive, negative):
    '''
    The function calculates the distance between anchor, positive and anchor, negative images. The difference 
    between the pos_distance, neg_distance was calculated and tuned according to the checking_label.
    '''
    pos_distance = F.pairwise_distance(anchor, positive, 2)
    neg_distance = F.pairwise_distance(anchor, negative, 2)
    return (pos_distance, neg_distance)

def predictive_value(conf_matrix):
    '''
    This function returns the positive predictive value and negative predictive value of a classifier.
    Parameters:
    
    1. conf_matrix : A Confusion matrix from the result of a classifier.
    '''
    ppv = (conf_matrix[0][0]/(conf_matrix[0][0] + conf_matrix[0][1]))*100
    npv = (conf_matrix[1][1]/(conf_matrix[1][0] + conf_matrix[1][1]))*100
    return (ppv, npv)

def train_and_eval(ref_model, train_dataloader, test_dataloader, criterion, optimizer):
    for epoch in trange(5):
    
        tr_loss = 0
        nb_tr_steps = 0
        ref_model.train()
        for step, batch in enumerate(train_dataloader):
            
            if device_gpu:
                anchor, positive, negative, target = batch[0].cuda(), batch[1].cuda(), batch[2].cuda(), batch[3].cuda()
            else:
                anchor, positive, negative, target = batch[0], batch[1], batch[2], batch[3]
            anchor, positive, negative, target = Variable(anchor), Variable(positive), Variable(negative), Variable(target)

            bs, c, h, w = anchor.size()
            anchor_input = anchor.view(-1, c, h, w)
            bs, c, h, w = positive.size()
            positive_input = positive.view(-1, c, h, w)
            bs, c, h, w = negative.size()
            negative_input = negative.view(-1, c, h, w)

            E1, E2, E3 = ref_model(anchor_input, positive_input, negative_input)
            dist_E1_E2, dist_E1_E3 = triplet_distance(E1, E2, E3)

            if device_gpu:
                target = target.cuda()
                
            loss = criterion(dist_E1_E2, dist_E1_E3, target)
            tr_loss += loss
            nb_tr_steps += 1

            torch.autograd.set_detect_anomaly(True)
            optimizer.zero_grad()
            loss.backward(retain_graph = True)
            optimizer.step()

        print("Train loss: {}".format(tr_loss/nb_tr_steps))

        ref_model.eval()
        pred_list = []
        with torch.no_grad():
            
            for step, batch in enumerate(test_dataloader):
                if device_gpu:
                    anchor, positive, negative, target = batch[0].cuda(), batch[1].cuda(), batch[2].cuda(), batch[3].cuda()
                else:
                    anchor, positive, negative, target = batch[0], batch[1], batch[2], batch[3]
                anchor, positive, negative, target = Variable(anchor), Variable(positive), Variable(negative), Variable(target)

                bs, c, h, w = anchor.size()
                anchor_input = anchor.view(-1, c, h, w)
                bs, c, h, w = positive.size()
                positive_input = positive.view(-1, c, h, w)
                bs, c, h, w = negative.size()
                negative_input = negative.view(-1, c, h, w)

                E1, E2, E3 = ref_model(anchor_input, positive_input, negative_input)
                dist_E1_E2, dist_E1_E3 = triplet_distance(E1, E2, E3)

                for i in range(len(dist_E1_E2)):
                    if (dist_E1_E2[i] > dist_E1_E3[i]):
                        pred_list.append(1)
                    else:
                        pred_list.append(0)

    #     mod_test_target = []
    #     for i in test_target:
    #         if i == -1:
    #             mod_test_target.append(1)
    #         else:
    #             mod_test_target.append(0)
    
    # return (mod_test_target, pred_list)
    return pred_list


class DenseNet121ToDense(nn.Module):
    '''
    The architecture of the densenet121 is copied and the final classifier layer is removed.
    In the place of final classifier layer a dense layer is attached with PReLU activation.
    '''
    def __init__(self, out_size):
        super(DenseNet121ToDense, self).__init__()
        self.densenet121todense = torchvision.models.densenet121()
        num_ftrs = self.densenet121todense.classifier.in_features
        self.densenet121todense.classifier = nn.Sequential(
            nn.Linear(num_ftrs, 512),
            nn.PReLU(),
            nn.Linear(512, out_size)
        )
        
    def forward(self, x_1, x_2, x_3):
        x_1 = self.densenet121todense(x_1)
        x_2 = self.densenet121todense(x_2)
        x_3 = self.densenet121todense(x_3)
        return (x_1, x_2, x_3)


# def retrainModel(modelPath):
#     args = {
#         'train_size' : 100,
#         'test_size' : 100,
#         'out_size' : 128, # should be less than 512
#         'loss_margin' : 0.2,
#         'loss_p' : 2,
#         'batch_size' : 4 # do not change this
#     }

#     # Load model
#     class_names = ['No Finding', 'Enlarged Cardiomediastinum', 'Cardiomegaly', 'Lung Opacity', 
#                 'Lung Lesion', 'Edema', 'Consolidation', 'Pneumonia', 'Atelectasis', 'Pneumothorax', 
#                 'Pleural Effusion', 'Pleural Other', 'Fracture', 'Support Devices']

#     log_format = '%(levelname)s %(asctime)s - %(message)s'
#     logging.basicConfig(filename = '../logs/original_fsl.logs',
#                         level = logging.INFO,
#                         format = log_format,
#                         filemode = 'w')
#     logger = logging.getLogger()

#     ref_model = DenseNet121ToDense(args['out_size']).cuda()
#     ref_model = torch.nn.DataParallel(ref_model).cuda()
#     logger.info('FSL model is loaded on to GPU.')

#     for _class in class_names:
#     #     if _class == 'Pneumothorax':
#         actual_class = _class
#         pred_class = 'pred_'+_class
#         print ('Class Name : {}'.format(actual_class))
#         logger.info('Class Name : {}'.format(actual_class))
        
#         images, checking_label = make_train_triplets(_class, sample_train_data, 150)
#         test_images, test_checking_label, false_inference_index = make_test_triplets(_class, sample_train_data)

#         first_images, second_images, third_images, target = ref_train_image_triplets(images, checking_label)
#         test_first_images, test_second_images, test_third_images, test_target = ref_test_image_triplets(test_images, test_checking_label)

#         ref_train_data = TensorDataset(first_images, second_images, third_images, target)
#         train_dataloader = DataLoader(ref_train_data, batch_size = args['batch_size'], shuffle = True, num_workers = 8, pin_memory = True)

#         test_data = TensorDataset(test_first_images, test_second_images, test_third_images, test_target)
#         test_dataloader = DataLoader(test_data, batch_size = args['batch_size'], shuffle = False, num_workers = 8, pin_memory = True)

#         criterion = torch.nn.MarginRankingLoss(margin = args['loss_margin'])
#         optimizer = optim.Adam (model.parameters(), lr=0.0001, betas=(0.9, 0.999), eps=1e-08, weight_decay=1e-5)

#         mod_test_target, pred_list = train_and_eval(ref_model, train_dataloader, test_dataloader)

#         # Calculating the ROC-AUC Scores before and after the few shot learning algorithm.
#         sample_train_data_copy_pred_class = copy.deepcopy(sample_train_data[pred_class])
#         i = 0
#         for index in false_inference_index:
#             sample_train_data_copy_pred_class[index] = pred_list[i]
#             i += 1

#         sample_train_data['FSL_pred_'+_class] = sample_train_data_copy_pred_class

#         print ('Before FSL - ') 
#         logger.info('Before FSL - ')
#         ppv, npv = predictive_value(confusion_matrix(sample_train_data[actual_class], sample_train_data[pred_class]))
#         print ('Positive Predictive Value : {}'.format(ppv))
#         logger.info('Positive Predictive Value : {}'.format(ppv))
#         print ('Negative Predictive Value : {}'.format(npv))
#         logger.info('Negative Predictive Value : {}'.format(npv))

#         print ('After FSL - ')
#         logger.info('After FSL - ')
#         ppv, npv = predictive_value(confusion_matrix(sample_train_data[actual_class], sample_train_data_copy_pred_class))
#         print ('Positive Predictive Value : {}'.format(ppv))
#         logger.info('Positive Predictive Value : {}'.format(ppv))
#         print ('Negative Predictive Value : {}'.format(npv))
#         logger.info('Negative Predictive Value : {}'.format(npv))
#         print ('--------------------------------------')
#         logger.info('--------------------------------------')
        
# def retrainModel(modelPath):
args = {
    'train_size' : 100,
    'test_size' : 100,
    'out_size' : 128, # should be less than 512
    'loss_margin' : 0.2,
    'loss_p' : 2,
    'batch_size' : 4 # do not change this
}

# Load model
# class_names = ['No Finding', 'Enlarged Cardiomediastinum', 'Cardiomegaly', 'Lung Opacity', 
#             'Lung Lesion', 'Edema', 'Consolidation', 'Pneumonia', 'Atelectasis', 'Pneumothorax', 
#             'Pleural Effusion', 'Pleural Other', 'Fracture', 'Support Devices']

class_names = ['Atelectasis', 'Cardiomegaly', 'Effusion', 'Infiltration', 'Mass', 'Nodule', 'Pneumonia',
                'Pneumothorax', 'Consolidation', 'Edema', 'Emphysema', 'Fibrosis', 'Pleural_Thickening', 'Hernia']


log_format = '%(levelname)s %(asctime)s - %(message)s'
logging.basicConfig(filename = '../logs/original_fsl.logs',
                    level = logging.INFO,
                    format = log_format,
                    filemode = 'w')
logger = logging.getLogger()

if device_gpu:
    ref_model = DenseNet121ToDense(args['out_size']).cuda()
    ref_model = torch.nn.DataParallel(ref_model).cuda()
    logger.info('FSL model is loaded on to GPU.')
else:
    ref_model = DenseNet121ToDense(args['out_size'])
    ref_model = torch.nn.DataParallel(ref_model)
    logger.info('FSL model is loaded on to CPU.')


data_l = []

for _class in class_names:
#     if _class == 'Pneumothorax':
    score_l = []
    actual_class = _class
    pred_class = 'pred_'+_class
    print ('Class Name : {}'.format(actual_class))
    logger.info('Class Name : {}'.format(actual_class))
    
    score_l.append(actual_class)
    
    images, checking_label = make_train_triplets(_class, sample_train_data, 5)
    test_images, test_checking_label, false_inference_index = make_test_triplets(_class, sample_train_data)

    first_images, second_images, third_images, target = ref_train_image_triplets(images, checking_label)
    test_first_images, test_second_images, test_third_images, test_target = ref_test_image_triplets(test_images, test_checking_label)

    ref_train_data = TensorDataset(first_images, second_images, third_images, target)
    train_dataloader = DataLoader(ref_train_data, batch_size = args['batch_size'], shuffle = True, num_workers = 8, pin_memory = True)

    test_data = TensorDataset(test_first_images, test_second_images, test_third_images, test_target)
    test_dataloader = DataLoader(test_data, batch_size = args['batch_size'], shuffle = False, num_workers = 8, pin_memory = True)

    criterion = torch.nn.MarginRankingLoss(margin = args['loss_margin'])
    optimizer = optim.Adam(ref_model.parameters(), lr=0.0001, betas=(0.9, 0.999), eps=1e-08, weight_decay=1e-5)

    # mod_test_target, pred_list = train_and_eval(ref_model, train_dataloader, test_dataloader, criterion, optimizer)
    pred_list = train_and_eval(ref_model, train_dataloader, test_dataloader, criterion, optimizer)

    

    # Calculating the ROC-AUC Scores before and after the few shot learning algorithm.
    sample_train_data_copy_pred_class = copy.deepcopy(sample_train_data[pred_class])
    i = 0
    for index in false_inference_index:
        sample_train_data_copy_pred_class[index] = pred_list[i]
        i += 1


    sample_train_data['FSL_pred_'+_class] = sample_train_data_copy_pred_class


    print ('Before FSL - ') 
    logger.info('Before FSL - ')
    ppv, npv = predictive_value(confusion_matrix(sample_train_data[actual_class], sample_train_data[pred_class]))
    print ('Positive Predictive Value : {}'.format(ppv))
    logger.info('Positive Predictive Value : {}'.format(ppv))
    print ('Negative Predictive Value : {}'.format(npv))
    logger.info('Negative Predictive Value : {}'.format(npv))
    
    score_l = score_l + [ppv, npv]

    print ('After FSL - ')
    logger.info('After FSL - ')
    ppv, npv = predictive_value(confusion_matrix(sample_train_data[actual_class], sample_train_data_copy_pred_class))
    print ('Positive Predictive Value : {}'.format(ppv))
    logger.info('Positive Predictive Value : {}'.format(ppv))
    print ('Negative Predictive Value : {}'.format(npv))
    logger.info('Negative Predictive Value : {}'.format(npv))
    print ('--------------------------------------')
    logger.info('--------------------------------------')
    
    score_l = score_l + [ppv, npv]
    print(score_l)
    data_l.append(score_l)
    print(data_l)
    
df_score = pd.DataFrame(data_l, columns = ["Class", "ppv_before", "npv_before", "ppv_afterFSL", "npv_afterFSL"])