from marshmallow import Schema, fields

class COCOSchema(Schema):
    cls = fields.String()
    x1 = fields.Number()
    y1 = fields.Number()
    x2 = fields.Number()
    y2 = fields.Number()
    score = fields.Number()